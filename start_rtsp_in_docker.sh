#!/bin/bash
# Demo GStreamer Sample Application for RTSP Streaming to Kinesis Video Streams
# To be run inside the Docker container

cd /opt/amazon-kinesis-video-streams-producer-sdk-cpp/build/

# Start the demo rtsp application to send video streams
export LD_LIBRARY_PATH=/opt/amazon-kinesis-video-streams-producer-sdk-cpp/open-source/local/lib:$LD_LIBRARY_PATH

# $4 for STREAM_NAME and $3 is RTSP_URL
# AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 ./kinesis_video_gstreamer_sample_app $4 $3


gst-launch-1.0 rtspsrc location=$3 short-header=TRUE ! rtph264depay ! video/x-h264, width=640,height=480,framerate=15/1, format=avc,alignment=au ! h264parse ! kvssink stream-name=$4 storage-size=512 access-key=$1 secret-key=$2 aws-region=$5

